<?php
class users_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function get_users($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('users');
			return $query->result_array();
		}

//        $this->db->join('carregamentos', 'carregamentos.CardID = users.CardID');//, 'left'
        $this->db->order_by("SaldoUpdate", "desc");
		$query = $this->db->get_where('users', array('users.CardID' => $slug));
		return $query->row_array();
	}

    public function get_users_by_id($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            $query = $this->db->get('users');
            return $query->result_array();
        }
        $query = $this->db->get_where('users', array('users.id' => $slug));
        return $query->row_array();
    }


    public function get_usersAfter($slug = FALSE)
    {

        if ($slug === FALSE)
        {
            $slug = ""; //Para não tirar nenhum
            $this->db->select('*');
            $this->db->order_by("SaldoUpdate", "desc");
            $query = $this->db->get_where('users', array('users.CardID' => $slug));
            return $query->result_array();
        }
//        log_message('error', " no get_usersAfter ");
//        log_message('error', $slug);
        $this->db->order_by("SaldoUpdate", "desc");
        $query = $this->db->get_where('users', array('users.SaldoUpdate >' => $slug));
        return $query->result_array();
    }

    public function set_users()
	{
		$this->load->helper('url');
		
		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'username' => $this->input->post('username'),
			//'slug' => $slug,
			//'CardID' => $this->input->post('CardID')
            'password' => MD5($this->input->post('password'))
		);

        // @todo fazer um lindo no erro do ja existe este utilizador

		return $this->db->insert('users', $data);
	}

    function login($username, $password)
    {
        $this -> db -> select('id, username, password');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
