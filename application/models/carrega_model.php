<?php
class carrega_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function get_carrega($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$this->db->select('*');
			$this->db->from('carregamentos');

            $this->db->order_by("Quando", "desc");
			$query = $this->db->get();
			return $query->result_array();
		}

        $this->db->order_by("Quando", "desc");
        $query = $this->db->get_where('carregamentos', array('CardID' => $slug));
		return $query->result_array();
	}
	public function set_carrega($CardIDt)
	{
		$this->load->helper('url');
        $this->load->helper('date');
		
//		$slug = url_title($this->input->post('Valor'), 'Valor', TRUE);

        log_message('error', "o valor:");
        log_message('error', $this->input->post('Valor'));

        log_message('error', "Validade:");
        log_message('error', $this->input->post('Validade'));

        $tserver = unix_to_human(now(), TRUE, 'eu'); // Euro time with seconds

        log_message('error', "O tempo no servidor:");
        log_message('error', $tserver);

        $validade =  $this->input->post('Validade');

        $validade =

		$data = array(
			'SaldoCarregado' => $this->input->post('Valor'),
			'Quando' => $tserver,
			'CardID' => $CardIDt,
            'Validade' => $this->input->post('Validade')
//            $this->input->post('CardID')
		);

        // @todo fazer um lindo no erro do ja existe este utilizador

        //E actualiza a tabela users com a data do ultimo carregamento
        $this->db->insert('carregamentos', $data);

        $dataUpdate = array(
            'CardID' => $CardIDt,
            'SaldoUpdate' => $tserver,
            'Validade' => $this->input->post('Validade')
        );

        $this->db->where('CardID', $CardIDt);
        $this->db->update('users', $dataUpdate);

        $this->db->select_sum('SaldoCarregado');
        $querySaldoCarregado = $this->db->get_where('carregamentos', array('CardID' => $CardIDt));

        $this->db->select_sum('Saldo_Cobrado');
        $queryCobrado = $this->db->get_where('entradas', array('CardID' => $CardIDt));

        $dataUsers = array(
            'SaldoUpdate' => $tserver,
            'Saldo' => $querySaldoCarregado->row()->SaldoCarregado - $queryCobrado->row()->Saldo_Cobrado
        );

        $this->db->where('CardID', $CardIDt);
        $this->db->update('users', $dataUsers);

//        echo "Saldo Carregado "+ $querySaldoCarregado->row()->SaldoCarregado;
		return true;
	}
}


//rascunho
//        $now = time();
//        $gmt = local_to_gmt($now);
//        $gmt = unix_to_human($now, TRUE, 'eu'); // Euro time with seconds


//        $timezone = 'UTC';
//        $daylight_saving = TRUE;
//        $currentTime =  gmt_to_local(now(), $timezone, $daylight_saving);

//        $dif = timezones('UTC');
//        log_message('error', "o tempo  no servidor:");
//        log_message('error', $tserver);

//        $gmt = unix_to_human($currentTime, TRUE, 'eu'); // Euro time with seconds

//        log_message('error', "o tempo  UTC:");
//        log_message('error', $gmt);