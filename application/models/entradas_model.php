<?php
class entradas_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
    public function EntradasDeUsers(){
        $this->db->select('*');
        $this->db->from('entradas');
        $this->db->join('users', 'entradas.CardID = users.CardID');
        $this->db->order_by("entradas.LastIn_Out", "desc");
        $this->db->group_by('entradas.CardID');
        $query = $this->db->get();

        return $query;

    }
    public function Entradas_Coworkers(){
        $this->db->select('*');
        $this->db->from('entradas');
        $this->db->join('coworkers', 'entradas.CardID = coworkers.CardID');
        $this->db->order_by("entradas.LastIn_Out", "desc");
        $this->db->group_by('entradas.CardID');
        $query = $this->db->get();

        return $query;
    }
    public function Entradas(){
        $this->db->select('*');
        $this->db->from('entradas');
//        $this->db->join('users', 'entradas.CardID = users.CardID');
        $this->db->order_by("entradas.LastIn_Out", "desc");
//        $this->db->group_by('entradas.CardID');
        $query = $this->db->get();

        return $query;

    }
    public function record_count_EntradasDeUsers() {
        return $this->EntradasDeUsers()->num_rows();//$query->num_rows();
    }
    public function record_count_Entradas_Coworkers() {
        return $this->Entradas_Coworkers()->num_rows();//$query->num_rows();
    }

    public function record_count_Entradas() {
        return $this->Entradas()->num_rows();//$query->num_rows();
    }

    public function fetch_entradas_EntradasDeUsers($limit, $start, $CardID = FALSE) {
        $this->db->limit($limit, $start);

        if ($CardID === FALSE){
            $query = $this->get_entradas_de_users();

        }
        else
        {
            $query = $this->get_entradas_de_users($CardID);
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function fetch_entradas_Entradas($limit, $start, $CardID = FALSE) {
        $this->db->limit($limit, $start);

        if ($CardID === FALSE){
            $query = $this->get_entradas();

        }
        else
        {
            $query = $this->get_entradas($CardID);
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function fetch_entradas_Entradas_Coworkers($limit, $start, $CardID = FALSE) {
        $this->db->limit($limit, $start);

        if ($CardID === FALSE){
            $query = $this->get_entradas_de_coworkers();

        }
        else
        {
            $query = $this->get_entradas_de_coworkers($CardID);
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function get_entradas_de_coworkers($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            return $this->Entradas_Coworkers();
        }
        $this->db->order_by("LastIn_Out", "desc");
        $query = $this->db->get_where('entradas', array('CardID' => $slug));
        return $query;
    }


	public function get_entradas_de_users($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			return $this->EntradasDeUsers();
		}

        $this->db->order_by("LastIn_Out", "desc");
        $query = $this->db->get_where('entradas', array('CardID' => $slug));
		return $query;
	}
    public function get_entradas($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            return $this->Entradas();
        }

        $this->db->order_by("LastIn_Out", "desc");
        $query = $this->db->get_where('entradas', array('CardID' => $slug));
        return $query;
    }
	public function set_news()
	{
		$this->load->helper('url');
		
		$slug = url_title($this->input->post('title'), 'dash', TRUE);
		
		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text')
		);
		
		return $this->db->insert('news', $data);
	}
}