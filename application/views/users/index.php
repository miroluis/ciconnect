<div class="span6" data-original-title="" title="">
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>CardID</th>
            <th>USER</th>
            <th>Saldo</th>
            <th>Validade</th>
            <th>Carregado em:</th>
            <th>link</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $users_item): ?>
            <tr>
                <td>
                    <?php echo $users_item['id'] ?>
                </td>
                <td>
                    <?php echo $users_item['username'] ?>
                </td>
                <td>
                    <?php echo $users_item['Saldo'] ?>
                </td>
                <td>
                    <?php echo $users_item['Validade'] ?>
                </td>
                <td>
                    <?php echo $users_item['SaldoUpdate'] ?>
                </td>
                <td>
                    <a href="users/<?php echo $users_item['id'] ?>">Edit</a>
                </td>
            </tr>
        </tbody>
        <?php endforeach ?>
    </table>
    <p>
        <a class="btn btn-primary" href="users/create">Cria novo user</a>
    </p>
</div>