<head>
    <title>Utilizador criado com sucesso</title>
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
</head>

<style type="text/css"><!--
    html,body,table#pagelayout {
        height:100%;
        width:90%;
        margin:auto;
    }
    // -->
</style>

<table style="width: 100%;height: 100%">
    <tr>
        <td style="text-align: center; vertical-align: middle;">
            <h1  >Utilizador Criado com sucesso</h1>
            <br>
            <a class="btn btn-primary" href="<?php echo site_url('users') ?>">Voltar</a>
        </td>
    </tr>
</table>