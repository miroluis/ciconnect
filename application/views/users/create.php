<?php
/**
 * Created by JetBrains PhpStorm.
 * User: MiroFr
 * Date: 03-04-2013
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="span6" data-original-title="" title="">
    <h2>Cria um novo utilizador</h2>
    <?php echo validation_errors(); ?>
    <?php echo form_open('users/create') ?>
    <div class="control-group">
        <div class="controls">
            <label class="control-label" for="username">Nome</label>
            <input type="text" name="username" placeholder="Nome"/><br />
        </div>
        <div class="control-group">
            <div class="controls">
                <label for="password">Password</label>
                <input type="password" name="password" placeholder="password"/><br />
            </div>
        </div>
        <p></p>
        <button class="btn btn-primary" type="submit">Cria novo user</button>
        </form>
    </div>
</div>