<?php
/**
 * Created by JetBrains PhpStorm.
 * User: MiroFr
 * Date: 27-04-2013
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */

// RFC4627-compliant header
header('Content-type: application/json');

// Encode data
if(isset($response)) {
    echo json_encode($response);
//    json_encode
}
else
    echo json_encode(array('error' => true));

?>