<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <title><?php echo $title ?> - NFC Lock</title>

    <?php $this->load->helper('html'); ?>

    <link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon" />

    <link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/css/smoothness\jquery-ui-1.10.2.custom.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/smoothness\jquery-ui-1.10.2.custom.min.css') ?>" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/jquery-1.9.1.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui-1.10.2.custom.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui-1.10.2.custom.min.js') ?>"></script>

    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">

    <script type="text/javascript">
        $("document").ready(function() {
            $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
<!--    teste-->
</head>
<body>

<div class="page-header pagination-centered">
    <header>
            <h1>NFC Lock
                <small>
                    Mobile, Easy, Secure
                </small>
            </h1>
    </header>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <ul class="thumbnails">
                <a href="http:\\www.one2sw.com">
                    <?php  echo img('assets/img/logo-onesw.png');?>
                </a>
            </ul>
            <ul class="nav nav-list">
                <li class="nav-header">Menu</li>
<!--                <li class="active"><a href="#">Home</a></li>-->
<!--                <li><a href="#">Library</a></li>-->
<!--                <li ><a href="--><?php //echo site_url('entradas') ?><!--">Home</a></li>-->
<!--                'pages/view'-->
                <li><a href="<?php echo site_url('entradas') ?>">Lista de entradas</a></li>
                <li><a href="<?php echo site_url('entradas/poCoworker') ?>">Lista de entradas por cooworker</a></li>
                <li><a href="<?php echo site_url('users') ?>">Lista de users</a></li>
                <li><a href="<?php echo site_url('coworkers') ?>">Lista de coworkers</a></li>
                <li><a href="<?php echo site_url('home/logout') ?>">Logout</a></li>
            </ul>
        </div>



