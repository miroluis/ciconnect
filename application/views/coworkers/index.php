<div class="span6" data-original-title="" title="">
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>CardID</th>
            <th>Coworker</th>
            <th>Saldo</th>
            <th>Validade</th>
            <th>Carregado em:</th>
            <th>link</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($coworkers as $coworker_item): ?>
            <tr>
                <td>
                    <?php echo $coworker_item['CardID'] ?>
                </td>
                <td>
                    <?php echo $coworker_item['username'] ?>
                </td>
                <td>
                    <?php echo $coworker_item['Saldo'] ?>
                </td>
                <td>
                    <?php echo $coworker_item['Validade'] ?>
                </td>
                <td>
                    <?php echo $coworker_item['SaldoUpdate'] ?>
                </td>
                <td>
                    <a href="users/<?php echo $coworker_item['CardID'] ?>">Edit</a>
                </td>
            </tr>
        </tbody>
        <?php endforeach ?>
    </table>
    <p>
        <a class="btn btn-primary" href="coworkers/create">Cria novo coworker</a>
    </p>
</div>