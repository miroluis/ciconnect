<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resetpw extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    function index()
    {
        $this->load->helper(array('form'));
        $this->load->helper('string');
        $this->load->view('resetpw_view');
    }

}

?>