<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Entradas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('entradas_model');
        $this->load->helper('url');
        $this->load->library("pagination");
	}

	public function index()
	{
        $config = array();
        $config["base_url"] = base_url() . "index.php/entradas/index";//countriescontroller/example1";//example1";//welcome
        $config["total_rows"] = $this->entradas_model->record_count_Entradas();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

//        $config['first_link'] = 'First';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->entradas_model->fetch_entradas_Entradas($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        //a parte de cima diz respeito à paginação

		$data['entradas'] = $this->entradas_model->get_entradas_de_users()->result_array();

		$data['title'] = 'Lista de entradas';

		$this->load->view('templates/header', $data);
		$this->load->view('entradas/index', $data);
		$this->load->view('templates/footer');
	}
    public function poCoworker()
    {
        $config = array();
        $config["base_url"] = base_url() . "index.php/entradas/index";//countriescontroller/example1";//example1";//welcome
        $config["total_rows"] = $this->entradas_model->record_count_Entradas_Coworkers();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

//        $config['first_link'] = 'First';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->entradas_model->fetch_entradas_Entradas_Coworkers($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        //a parte de cima diz respeito à paginação

        $data['entradas'] = $this->entradas_model->get_entradas_de_users()->result_array();

        $data['title'] = 'Lista de entradas';

        $this->load->view('templates/header', $data);
        $this->load->view('entradas/lista_coworkers', $data);
        $this->load->view('templates/footer');
    }
	public function view($slug)
	{
        $config = array();//http://localhost/CodeIgnitermiroelectronics/index.php/entradas/view/fd0fe61d
        $config["base_url"] = base_url() . "index.php/entradas/view/".$slug;//countriescontroller/example1";//example1";//welcome
        $config["total_rows"] = $this->entradas_model->get_entradas_de_users($slug)->num_rows();
        $config["per_page"] = 15;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

//        $config['first_link'] = 'First';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        log_message('error', 'página é ');
        log_message('error', $page);
        $data["results"] = $this->entradas_model->fetch_entradas_EntradasDeUsers($config["per_page"], $page, $slug);//fetch_entradas_EntradasDeUsers
        $data["links"] = $this->pagination->create_links();

        //a parte de cima diz respeito à paginação

        if ($slug=="entradas"){

            redirect('entradas', 'refresh');

        }
        $data['entradas'] = $this->entradas_model->get_entradas_de_users($slug)->result_array();;
		if (empty($data['entradas']))
		{
			show_404();
		}

		$data['title'] = "Detail";

		$this->load->view('templates/header', $data);
		$this->load->view('entradas/view', $data);
		$this->load->view('templates/footer');
	}
	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = 'Create a news item';
		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');
		
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('news/create');
			$this->load->view('templates/footer');
			
		}
		else
		{
			$this->news_model->set_news();
			$this->load->view('news/success');
		}
	}
}