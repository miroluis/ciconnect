<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->view('home_view', $data);
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function logout()
    {
        log_message('error', "no log out chego aqui1");
//        log_message('error', $gmt);
        // Kill session data
        $this->userdata = array();
        $this->session->unset_userdata('logged_in');
        log_message('error', "no log out chego aqui2");
        session_destroy();
        log_message('error', "no log out chego aqui3");
        redirect('home', 'refresh');
    }

}
?>