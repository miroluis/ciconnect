<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Syncuser extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('carrega_model');
        $this->load->helper('array');
        $this->load->helper('url');
    }

    public function allUsers()
    {
        //A correr diariamente
        $users['users'] = $this->users_model->get_users();

        // Build our view's data object
        $data = array('response' => $users);
        $this->load->view('json', $data);
    }
    public function theUser($CardID)
    {
        //corre após a passagem do cartão
        $users['users'] = $this->users_model->get_users($CardID);

        // Build our view's data object
        $data = array('response' => $users);
        $this->load->view('json', $data);
    }
    public function afterUser($date)
    {
        $date = str_replace("%20"," ", $date);
        $date = str_replace("%22"," ", $date);
        $date = str_replace('"'," ", $date);
//        echo "a data: <br>";
//        echo $date;
        //corre após a passagem do cartão
        $users['users'] = $this->users_model->get_usersAfter($date);

        // Build our view's data object
        $data = array('response' => $users);
        $this->load->view('json', $data);
    }

}

//http://localhost/CodeIgniter/index.php/syncuser/afterUser/2013-02-28%2013:22:38
//http://nfc-portugal.pt/fechaduranfc/index.php/syncuser/afterUser/2013-02-28%2013:22:38
//http://nfc-portugal.pt/fechaduranfc/index.php/syncuser/afterUser/"+datacorrigida+""
//http://localhost/CodeIgniter/index.php/syncuser/afterUser/2013-02-04
//http://localhost/CodeIgniter/index.php/syncuser/theUser/321/123
//public function theUser($CardID, $teste)
//{
//    $users['users'] = $this->users_model->get_users($CardID);
//    $users['teste'] = $this->users_model->get_users($teste);
//
//É USADO ASSIM
//http://localhost/CodeIgniter/index.php/syncuser/index


//            log_message('error', " deu certo");
//            log_message('error', $slug);

//        log_message('error', "slug é:");
//        log_message('error', $slug);
//        $slug = $this->CardIDact;

//        log_message('error', " foi para algures");
//        log_message('error', $slug);



//        $data['users_item'] = $this->users_model->get_users($slug);
?>