<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Users extends CI_Controller {

    public $CardIDact;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
        $this->load->model('carrega_model');
        $this->load->helper('array');
        $this->load->helper('url');
        $this->output->nocache();
	}

	public function index()
	{
		$data['users'] = $this->users_model->get_users();
		$data['title'] = 'A Lista de users';

		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug)
	{
        $this->load->helper('form');
        $this->load->library('form_validation');
		$data['users_item'] = $this->users_model->get_users_by_id($slug);

        if (empty($data['users_item']))
        {
            show_404();
        }

        $cardid = $data['users_item']['CardID'];
        $this->CardIDact = $cardid;//para passar a variavel desta função para a do carrega

//define-se $data['carregamentos'] e vai-se buscar ao model dos carregamentos o slug contem o id do user acima
//        $data['carregamentos'] = $this->blog_model->get_post($id);

        $data['carrega_item'] = $this->carrega_model->get_carrega($cardid);
        if (empty($data['carrega_item']))
        {
            log_message('error', "carregamentos vazio:");
            log_message('error', $cardid);
        }

		$data['title'] = $data['users_item']['CardID'];

		$this->load->view('templates/header', $data);
		$this->load->view('users/view', $data);
		$this->load->view('templates/footer');
	}
    public function carrega($CardID)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Carrega Valor';

        $data['users_item'] =  $this->users_model->get_users($CardID);

        $this->form_validation->set_rules('Valor', 'Valor', 'required');
        $this->form_validation->set_rules('Validade', 'Validade', 'required');

        if ($this->form_validation->run() === FALSE)
        {
//            $this->load->view('templates/header', $data);
            $this->load->view('users/carreganovamente', $data);
//            $this->load->view('templates/footer');
        }
        else
        {
            $this->carrega_model->set_carrega($CardID);
            $this->load->view('users/carrega_success', $data);
        }
    }
	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = 'Cria um novo utilizador';
		
		$this->form_validation->set_rules('username', 'Nome', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);	
			$this->load->view('users/create');
			$this->load->view('templates/footer');
			
		}
		else
		{
			$this->users_model->set_users();
			$this->load->view('users/success');
		}
	}
}

