<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: MiroFr
 * Date: 18-04-2013
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */

class Enviapw extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('users_model','',TRUE);
    }

    function index()
    {
        //@todo falta indicar que a data de validade é inclusive. isto porque ela nao contem a hora
        //@todo verifica se user tem email
        //@todo envia email para aquele user
        $this->load->view('welcome_message');
//        //This method will have the credentials validation
//        $this->load->library('form_validation');
//
//        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
//
//        if($this->form_validation->run() == FALSE)
//        {
//            //Field validation failed.&nbsp; User redirected to login page
//            $this->load->view('login_view');
//
//        }
//        else
//        {
//            //Go to private area
////            redirect('home', 'refresh');
//            redirect('pages/view');
//        }

    }

    function check_database($password)
    {
        //Field validation succeeded.&nbsp; Validate against database
        $username = $this->input->post('username');

        //query the database
        $result = $this->users_model->login($username, $password);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}
?>
